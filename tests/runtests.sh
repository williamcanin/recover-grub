#!/usr/bin/env bash

## Run tests at the root of the project.
# PYTHONPATH="." python3 tests/tests.py

## Run tests inside the 'tests' folder.
PYTHONPATH=".." python3 tests.py
